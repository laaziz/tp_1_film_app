//
//  Movies.swift
//  filmApp
//
//  Created by piman on 4/10/20.
//  Copyright © 2020 piman. All rights reserved.
//

import Foundation

struct Movies : Decodable {
    var results: [Movie]
}
