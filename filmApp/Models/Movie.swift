//
//  Movie.swift
//  filmApp
//
//  Created by piman on 4/10/20.
//  Copyright © 2020 piman. All rights reserved.
//

import Foundation

struct Movie : Identifiable, Decodable {
    var id : Int
    var popularity: Double
    var vote_count: Int
    var poster_path: String
    var original_title: String
    var title: String
    var vote_average: Double
    var overview: String
    var release_date: String
}

