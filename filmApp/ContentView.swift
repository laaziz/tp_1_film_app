//
//  ContentView.swift
//  filmApp
//
//  Created by piman on 4/8/20.
//  Copyright © 2020 piman. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ListMoviesView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
