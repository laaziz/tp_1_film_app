//
//  ListMoviesView.swift
//  filmApp
//
//  Created by piman on 4/10/20.
//  Copyright © 2020 piman. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct ListMoviesView: View {
    @ObservedObject var obj = NetworkManager()
    let BASE_IMAGE_URI = "https://image.tmdb.org/t/p/original/"
    var body: some View {
        NavigationView {
            List(obj.movies) { movie in
                NavigationLink(destination: DetailView(movie: movie)) {
                    HStack {
                        WebImage(url: URL(string: "\(self.BASE_IMAGE_URI)\(movie.poster_path)"))
                            .resizable()
                            .placeholder {
                                Rectangle().foregroundColor(.white)
                        }
                            .indicator(.activity) // Activity Indicator
                            .animation(.easeInOut(duration: 0.5)) // Animation Duration
                            .transition(.fade) // Fade Transition
                            .scaledToFit()
                            .frame(width: 100, height: 130, alignment: .center)
                        VStack(alignment: .leading, spacing: 5) {
                            Text(movie.title)
                                .font(.system(size: 16))
                                .bold()
                                .foregroundColor(Color.black)
                            Text(movie.release_date)
                                .font(.system(size: 12))
                                .foregroundColor(Color.gray)
                            HStack(spacing: 5) {
                                Image(systemName: "hand.thumbsup.fill").font(.system(size: 12))
                                Text("\(movie.vote_count)")
                                    .font(.system(size: 12)).foregroundColor(Color.gray)
                            }
                            HStack(spacing: 5) {
                                Text("Popularity").font(.system(size: 12)).bold()
                                Text("\(movie.popularity)")
                                    .font(.system(size: 12)).foregroundColor(Color.gray)
                            }
                            
                        }
                    }
                }
                
            }.navigationBarTitle("Film App")
        }
        
    }
}

struct ListMoviesView_Previews: PreviewProvider {
    static var previews: some View {
        ListMoviesView()
    }
}
