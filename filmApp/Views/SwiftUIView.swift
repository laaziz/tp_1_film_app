//
//  SwiftUIView.swift
//  filmApp
//
//  Created by piman on 4/10/20.
//  Copyright © 2020 piman. All rights reserved.
//

import SwiftUI

struct SwiftUIView: View {
    @ObservedObject var obj = NetworkManager()
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
