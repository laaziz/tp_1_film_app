//
//  DetailView.swift
//  filmApp
//
//  Created by piman on 4/23/20.
//  Copyright © 2020 piman. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailView: View {
    var movie: Movie
    let BASE_IMAGE_URI = "https://image.tmdb.org/t/p/original/"
    var body: some View {
        VStack {
            Spacer()
            RoundedShadowView(movie: movie)
                        .background(Color.white)
                        .cornerRadius(8)
                        .padding(10)
                        .padding(.bottom, 20)
                        .shadow(color: Color.black, radius: 20)
            
        }.edgesIgnoringSafeArea(.all)
            .background(
                WebImage(url: URL(string: "\(self.BASE_IMAGE_URI)\(movie.poster_path)"))
                    .resizable()
                    .placeholder {
                        Rectangle().foregroundColor(.white)
                }
                .indicator(.activity)
                .frame(width: UIScreen.main.bounds.width, height:  UIScreen.main.bounds.height + 200)
        )
        
        
    }
}

struct RoundedShadowView: View
{
    let movie: Movie
    var body: some View
    {
        VStack(alignment: HorizontalAlignment.leading, spacing: 10.0)
        {
       
            
            Text("description")
                .padding(8)
                .foregroundColor(.primary)
            
            Text("\(movie.overview)")
                .padding(EdgeInsets(top: 0.0, leading: 8.0, bottom: 8.0, trailing: 8.0))
                .lineLimit(nil)
                .lineSpacing(5)
                .font(.footnote)
                .foregroundColor(.secondary)
            
        }
    }
}
