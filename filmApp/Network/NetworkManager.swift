//
//  NetworkManager.swift
//  filmApp
//
//  Created by piman on 4/8/20.
//  Copyright © 2020 piman. All rights reserved.
//


import Alamofire
import Foundation
import Combine
import SwiftyJSON

class NetworkManager: ObservableObject {
    @Published var movies = [Movie]()
    @Published var loading = false
    private let api_key = "f465316d1fa95809aa24f49234a558fd"
    private let api_url_base = "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key="
    init() {
        loading = true
        loadDataByAlamofire()
    }
    
    private func loadDataByAlamofire() {
        AF.request("\(api_url_base)\(api_key)").responseData { (data) in
            let json = try! JSON(data: data.data!)
            for i in json["results"] {
                self.movies.append(Movie(
                    id: i.1["id"].intValue,
                    popularity: i.1["popularity"].doubleValue,
                    vote_count: i.1["vote_count"].intValue,
                    poster_path: i.1["poster_path"].stringValue,
                    original_title: i.1["original_title"].stringValue,
                    title: i.1["title"].stringValue,
                    vote_average: i.1["vote_average"].doubleValue,
                    overview: i.1["overview"].stringValue,
                    release_date: i.1["release_date"].stringValue)
                )
            }            
        }
    }
 
}

